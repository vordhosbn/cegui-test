#include "CEGUI/CEGUI.h"
#include "CEGUI/DefaultResourceProvider.h"
#include "CEGUI/RendererModules/OpenGL/GLRenderer.h"
#include "GL\glfw.h"
#include "WinMain.h"

#include <windows.h>
#include <stdlib.h>
#include <string.h>

static bool windowSizeChange = true;
static bool isQuitting = false;
static int windowWidth = 1024;
static int windowHeight = 768;



CEGUI::Key::Scan GlfwToCeguiKey(int glfwKey)
{
    CEGUI::Key::Scan retVal = CEGUI::Key::Unknown;
    switch (glfwKey)
    {
    case GLFW_KEY_ESC: retVal = CEGUI::Key::Escape;
        break;
    case GLFW_KEY_F1: retVal = CEGUI::Key::F1;
        break;
    case GLFW_KEY_F2: retVal = CEGUI::Key::F2;
        break;
    case GLFW_KEY_F3: retVal = CEGUI::Key::F3;
        break;
    case GLFW_KEY_F4: retVal = CEGUI::Key::F4;
        break;
    case GLFW_KEY_F5: retVal = CEGUI::Key::F5;
        break;
    case GLFW_KEY_F6: retVal = CEGUI::Key::F6;
        break;
    case GLFW_KEY_F7: retVal = CEGUI::Key::F7;
        break;
    case GLFW_KEY_F8: retVal = CEGUI::Key::F8;
        break;
    case GLFW_KEY_F9: retVal = CEGUI::Key::F9;
        break;
    case GLFW_KEY_F10: retVal = CEGUI::Key::F10;
        break;
    case GLFW_KEY_F11: retVal = CEGUI::Key::F11;
        break;
    case GLFW_KEY_F12: retVal = CEGUI::Key::F12;
        break;
    case GLFW_KEY_F13: retVal = CEGUI::Key::F13;
        break;
    case GLFW_KEY_F14: retVal = CEGUI::Key::F14;
        break;
    case GLFW_KEY_F15: retVal = CEGUI::Key::F15;
        break;
    case GLFW_KEY_UP: retVal = CEGUI::Key::ArrowUp;
        break;
    case GLFW_KEY_DOWN: retVal = CEGUI::Key::ArrowDown;
        break;
    case GLFW_KEY_LEFT: retVal = CEGUI::Key::ArrowLeft;
        break;
    case GLFW_KEY_RIGHT: retVal = CEGUI::Key::ArrowRight;
        break;
    case GLFW_KEY_LSHIFT: retVal = CEGUI::Key::LeftShift;
        break;
    case GLFW_KEY_RSHIFT: retVal = CEGUI::Key::RightShift;
        break;
    case GLFW_KEY_LCTRL: retVal = CEGUI::Key::LeftControl;
        break;
    case GLFW_KEY_RCTRL: retVal = CEGUI::Key::RightControl;
        break;
    case GLFW_KEY_LALT: retVal = CEGUI::Key::LeftAlt;
        break;
    case GLFW_KEY_RALT: retVal = CEGUI::Key::RightAlt;
        break;
    case GLFW_KEY_TAB: retVal = CEGUI::Key::Tab;
        break;
    case GLFW_KEY_ENTER: retVal = CEGUI::Key::Return;
        break;
    case GLFW_KEY_BACKSPACE: retVal = CEGUI::Key::Backspace;
        break;
    case GLFW_KEY_INSERT: retVal = CEGUI::Key::Insert;
        break;
    case GLFW_KEY_DEL: retVal = CEGUI::Key::Delete;
        break;
    case GLFW_KEY_PAGEUP: retVal = CEGUI::Key::PageUp;
        break;
    case GLFW_KEY_PAGEDOWN: retVal = CEGUI::Key::PageDown;
        break;
    case GLFW_KEY_HOME: retVal = CEGUI::Key::Home;
        break;
    case GLFW_KEY_END: retVal = CEGUI::Key::End;
        break;
    case GLFW_KEY_KP_ENTER: retVal = CEGUI::Key::NumpadEnter;
        break;
    default: retVal = CEGUI::Key::Unknown;
        break;
    }
    return retVal;
}

CEGUI::MouseButton GlfwToCeguiMouseButton(int glfwButton)
{
    CEGUI::MouseButton retVal = CEGUI::NoButton;
    switch (glfwButton)
    {
    case GLFW_MOUSE_BUTTON_LEFT: retVal = CEGUI::LeftButton;
        break;
    case GLFW_MOUSE_BUTTON_RIGHT: retVal = CEGUI::RightButton;
        break;
    case GLFW_MOUSE_BUTTON_MIDDLE: retVal = CEGUI::MiddleButton;
        break;
    default: retVal = CEGUI::NoButton;
        break;
    }
    return retVal;
}

void GLFWCALL glfwKeyCallback(int key, int action)
{
    CEGUI::Key::Scan ceguiKey = GlfwToCeguiKey(key);

    if (action == GLFW_PRESS)
    {
        // injectKeyDown(ceguiKey);
    }
    else if (action == GLFW_RELEASE)
    {
        // injectKeyUp(ceguiKey);
    }
}
void GLFWCALL glfwCharCallback(int character, int action)
{
    if (action == GLFW_PRESS)
    {
        // injectChar(character);
    }
}
void GLFWCALL glfwMouseButtonCallback(int key, int action)
{
    CEGUI::MouseButton ceguiMouseButton = GlfwToCeguiMouseButton(key);

    if (action == GLFW_PRESS)
    {
        // injectMouseButtonDown(ceguiMouseButton);
    }
    else if (action == GLFW_RELEASE)
    {
        // injectMouseButtonUp(ceguiMouseButton);
    }
}
void GLFWCALL glfwMouseWheelCallback(int position)
{
    static int lastPosition = 0;
    // injectMouseWheelChange(position - lastPosition);
    lastPosition = position;
}
void GLFWCALL glfwMousePosCallback(int x, int y)
{
    // injectMousePosition(x, y);
}

int GLFWCALL glfwWindowCloseCallback(void)
{
    isQuitting = true;
    return 0;
}
void GLFWCALL glfwWindowResizeCallback(int width, int height)
{
    windowSizeChange = true;
    windowWidth = width;
    windowHeight = height;
}

int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow) 
{
	int retVal = -1;

    // init GLFW library
    if (!glfwInit())
    {
        //Error "Can't init GLFW"
    }
    else
    {
        glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 1);
        glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 2);

        // create an OpenGL window
        if (!glfwOpenWindow(windowWidth, windowHeight, 0, 0, 0, 0, 24, 8, GLFW_WINDOW))
        {
            //Error "Unable to create OpenGL window"
        }
        else
        {
            // Input callbacks of glfw for CEGUI
            glfwSetKeyCallback(glfwKeyCallback);
            glfwSetCharCallback(glfwCharCallback);
            glfwSetMouseButtonCallback(glfwMouseButtonCallback);
            glfwSetMouseWheelCallback(glfwMouseWheelCallback);
            glfwSetMousePosCallback(glfwMousePosCallback);

            //Window callbacks
            glfwSetWindowCloseCallback(glfwWindowCloseCallback);
            glfwSetWindowSizeCallback(glfwWindowResizeCallback);
            windowSizeChange = false; //The resize callback is being called immediately after setting it in this version of glfw
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

            // set window title
            glfwSetWindowTitle("Main window title");

            CEGUI::OpenGLRenderer * HDTrenderer = &CEGUI::OpenGLRenderer::bootstrapSystem();
            CEGUI::DefaultResourceProvider* rp = static_cast<CEGUI::DefaultResourceProvider*>(
                CEGUI::System::getSingleton().getResourceProvider());
            rp->setResourceGroupDirectory("schemes", "../../data/schemes/");
            rp->setResourceGroupDirectory("imagesets", "../../data/imagesets/");
            rp->setResourceGroupDirectory("fonts", "../../data/fonts/");
            rp->setResourceGroupDirectory("layouts", "../../data/layouts/");
            rp->setResourceGroupDirectory("looknfeels", "../../data/looknfeels/");
            rp->setResourceGroupDirectory("lua_scripts", "../../data/lua_scripts/");
            // set the default resource groups to be used
            CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
            CEGUI::Font::setDefaultResourceGroup("fonts");
            CEGUI::Scheme::setDefaultResourceGroup("schemes");
            CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
            CEGUI::WindowManager::setDefaultResourceGroup("layouts");
            CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");

            CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
            // Set the defaults
            CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");
            CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");
            CEGUI::Window* myRoot = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "_MasterRoot");
            CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(myRoot);

            CEGUI::Window *myImageWindow = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage", "PrettyWindow");
            // position a quarter of the way in from the top-left of parent.
            myImageWindow->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25f, 0.0f), CEGUI::UDim(0.25f, 0.0f)));
            // set size to be half the size of the parent
            myImageWindow->setSize(CEGUI::USize(CEGUI::UDim(0.5f, 0.0f), CEGUI::UDim(0.5f, 0.0f)));
            myImageWindow->setProperty("Image", "TaharezLook/CloseButtonNormal");

            CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(myImageWindow);
        }
        windowSizeChange = true;
        while (!isQuitting && glfwGetWindowParam(GLFW_OPENED))
        {
            if (windowSizeChange)
            {
                CEGUI::System::getSingleton().
                    notifyDisplaySizeChanged(
                        CEGUI::Sizef(static_cast<float>(windowWidth),
                            static_cast<float>(windowHeight)));
            }
        }
    }

	return retVal;
}
